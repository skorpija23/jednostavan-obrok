package com.jednostavan_obrok.dto;

public class LocationDTO {

    private String lat;
    private String lon;
	public LocationDTO(String lat, String lon) {
		super();
		this.lat = lat;
		this.lon = lon;
	}
	public LocationDTO() {
		super();
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
    
    
}
