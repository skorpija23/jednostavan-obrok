package com.jednostavan_obrok.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.jednostavan_obrok.model.Korisnik;
import com.jednostavan_obrok.model.Viberuser;
import com.jednostavan_obrok.repository.KorisnikRepository;
import com.jednostavan_obrok.repository.ViberUserRepository;
import com.jednostavan_obrok.service.ViberMessagesService;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.stereotype.Service;

import com.sun.istack.Nullable;

import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


@Service
public class ViberMessagesServiceImpl  implements ViberMessagesService {



	@Autowired
	private KorisnikRepository korisnikRepository;

	@Autowired
	private ViberUserRepository viberUserRepository;

	    @Autowired
	    private OkHttpClient client;

	@Value("${application.viber-bot.auth-token}")
    private String authToken;

    @Value("${application.viber-bot.name}")
    private String name;

    @Nullable
    @Value("${application.viber-bot.avatar:@null}")
    private String avatar;



	private final String url= "https://fresh-bat-58.loca.lt/login";





//	@Override
//	public Object save(String viberUserDto) {
//		// TODO Auto-generated method stub
//
//		JSONParser parse = new JSONParser();
//		JSONObject o;
//		try {
//			o = (JSONObject) parse.parse(viberUserDto);
//			ViberUser viberUser = new ViberUser();
//			if (o.containsKey("user")) {
//				JSONObject VUser = (JSONObject) o.get("user");
//				viberUser.setViberId((String) VUser.get("id"));
//				viberUser.setName((String) VUser.get("name"));
//			} else {
//				JSONObject VUser = (JSONObject) o.get("sender");
//				viberUser.setViberId((String) VUser.get("id"));
//				viberUser.setName((String) VUser.get("name"));
//
//			}
//
//			if (viberUserRepository.findByViberId(viberUser.getViberId()) != null) {
//				viberUser = viberUserRepository.findByViberId(viberUser.getViberId());
//			} else {
//				viberUser = viberUserRepository.save(viberUser);
//			}
//
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}





	@Override
	public void sendMessageToAll(String message) {
		// TODO Auto-generated method stub
	    System.out.println("Send message to all UPAO!");
		List<Viberuser> listaSvihUsera=viberUserRepository.findAll();
		for(Viberuser vu:listaSvihUsera) {
			String viberid=vu.getViberId();
			sendMessage(viberid,message);
			System.out.println("Viber id za slanje ==> "+viberid);
		}


	}


	private void sendMessage(String viberid, String message)  {
		// TODO Auto-generated method stub
		System.out.println("Upao u send message, viber id  "+viberid);
//		JSONObject jsonObject = new JSONObject();
//	    try {
//	        jsonObject.put( "auth_token", "4dc864b9b8a7df67-4cae15dcecfb98bc-71d87a4895f1e472");
//	    } catch (Exception e) {
//	        e.printStackTrace();
//	    }
	    JSONObject jsonSenderBody = new JSONObject();
	    try {
	    	jsonSenderBody.put( "name", "JednostavanObrok ");
	    	jsonSenderBody.put( "avatar", " ");

	    } catch (Exception e) {
	        e.printStackTrace();
	    }

	    JSONObject jsonMessageBody = new JSONObject();
	    try {
	    	jsonMessageBody.put( "receiver", viberid);
	    	jsonMessageBody.put( "min_api_version", 1);
	    	jsonMessageBody.put( "tracking_data", "tracking data");
	    	jsonMessageBody.put( "type", "text");
	    	jsonMessageBody.put( "text", message);
	    	jsonMessageBody.put("sender", jsonSenderBody);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	  System.out.println();
	      MediaType JSON = MediaType.parse("application/json; charset=utf-8");

	  System.out.println(jsonMessageBody.toJSONString());
	      @SuppressWarnings("deprecation")
		RequestBody body = RequestBody.create(JSON, jsonMessageBody.toString());
	      Headers headerbuild = Headers.of("X-Viber-Auth-Token", "4e6844368567e2f6-fea4c7f9fe202ae4-94683f090cff4bb0");

	      System.out.println(body.toString()+" body message");

	      Request requested = new Request.Builder()
	    		  .url("https://chatapi.viber.com/pa/send_message")
	                        .post(body)
	                        .headers(headerbuild)
	                        .build();
	      System.out.println(requested);


	      try {
	    	  Response response = client.newCall(requested).execute();
			  String resStr = response.body().string();
	          System.out.println(resStr);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public String sendMassageToAll(String json) {
	    StringBuilder str= new StringBuilder();
	    str.append(json);
	    return str.toString();
	}
	@Override
	public List<Korisnik> findAll() {
		return korisnikRepository.findAll();
	}

	@Override
	public void save(String viberUserDto) {

		JSONParser parse = new JSONParser();
		JSONObject o;
		try {
			o = (JSONObject) parse.parse(viberUserDto);
			Viberuser viberUser = new Viberuser();
			if (o.containsKey("user")) {
				JSONObject VUser = (JSONObject) o.get("user");
				viberUser.setViberId((String) VUser.get("id"));
				viberUser.setViberName((String) VUser.get("name"));
			} else {
				JSONObject VUser = (JSONObject) o.get("sender");
				viberUser.setViberId((String) VUser.get("id"));
				viberUser.setViberName((String) VUser.get("name"));

			}

			if (viberUserRepository.findByViberId(viberUser.getViberId()) != null) {
				viberUser = viberUserRepository.findByViberId(viberUser.getViberId());
			} else {
				viberUser = viberUserRepository.save(viberUser);
			}


		} catch (Exception e) {
			e.printStackTrace();
		}


	}

	public List<String> listIds() {
		List<Viberuser> listUser = viberUserRepository.findAll();
		List<String> listIds = new ArrayList<String>();
		for (Viberuser vu: listUser) {

				listIds.add(vu.getViberId());

		}
		return listIds;
	}


}
