package com.jednostavan_obrok.repository;

import com.jednostavan_obrok.model.Obrok;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ObrokRepository extends JpaRepository<Obrok,Integer> {
}
