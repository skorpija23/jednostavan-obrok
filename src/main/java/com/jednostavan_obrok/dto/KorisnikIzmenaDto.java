package com.jednostavan_obrok.dto;

public class KorisnikIzmenaDto {
    private int idKorisnik;
    private String ime;
    private String prezime;
    private String adresa;
    private String mesto;
    private int mobilni;
    private String email;
    private String username;
    private String staraLozinka;
    private String novaLozinka;

    public KorisnikIzmenaDto(int idKorisnik, String ime, String prezime, String adresa, String mesto, int mobilni, String email, String username, String staraLozinka, String novaLozinka) {
        this.idKorisnik = idKorisnik;
        this.ime = ime;
        this.prezime = prezime;
        this.adresa = adresa;
        this.mesto = mesto;
        this.mobilni = mobilni;
        this.email = email;
        this.username = username;
        this.staraLozinka = staraLozinka;
        this.novaLozinka = novaLozinka;
    }

    public KorisnikIzmenaDto() {
    }

    public int getIdKorisnik() {
        return idKorisnik;
    }

    public void setIdKorisnik(int idKorisnik) {
        this.idKorisnik = idKorisnik;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getMesto() {
        return mesto;
    }

    public void setMesto(String mesto) {
        this.mesto = mesto;
    }

    public int getMobilni() {
        return mobilni;
    }

    public void setMobilni(int mobilni) {
        this.mobilni = mobilni;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getStaraLozinka() {
        return staraLozinka;
    }

    public void setStaraLozinka(String staraLozinka) {
        this.staraLozinka = staraLozinka;
    }

    public String getNovaLozinka() {
        return novaLozinka;
    }

    public void setNovaLozinka(String novaLozinka) {
        this.novaLozinka = novaLozinka;
    }
}
