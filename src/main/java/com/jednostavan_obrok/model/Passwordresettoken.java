package com.jednostavan_obrok.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the passwordresettoken database table.
 * 
 */
@Entity
@NamedQuery(name="Passwordresettoken.findAll", query="SELECT p FROM Passwordresettoken p")
public class Passwordresettoken implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;



//	@Temporal(TemporalType.DATE)
//    @Temporal(TemporalType.TIMESTAMP)
//	private Date expiryDate;

	private String token;

	//bi-directional many-to-one association to Korisnik
//	@ManyToOne
	@OneToOne(targetEntity = Korisnik.class, fetch = FetchType.EAGER)
	@JoinColumn(name="korisnik_id_korisnik")
	private Korisnik korisnik;

	public Passwordresettoken() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getToken() {
		return this.token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Korisnik getKorisnik() {
		return this.korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public Passwordresettoken(int id, String token, Korisnik korisnik) {
		this.id = id;
		this.token = token;
		this.korisnik = korisnik;
	}

}