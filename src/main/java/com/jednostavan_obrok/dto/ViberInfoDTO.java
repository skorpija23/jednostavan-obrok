package com.jednostavan_obrok.dto;

import java.util.List;

public class ViberInfoDTO {
	  private String status;
	  private String status_message;
	    private String id ;
	    private String chat_hostname;
	    private String name;
	    private String uri;
	    private String icon;
	    private String category;
	    private String subcategory;
	    private  LocationDTO location;	    
	    private String country;
	    private String webhook; 
	  
	    private List<MembersDTO> members;	      
	    private String subscribers_count;
		public ViberInfoDTO(String status, String status_message, String id, String chat_hostname, String name,
				String uri, String icon, String category, String subcategory, LocationDTO location, String country,
				String webhook,  List<MembersDTO> listMembers,
				String subscribers_count) {
			super();
			this.status = status;
			this.status_message = status_message;
			this.id = id;
			this.chat_hostname = chat_hostname;
			this.name = name;
			this.uri = uri;
			this.icon = icon;
			this.category = category;
			this.subcategory = subcategory;
			this.location = location;
			this.country = country;
			this.webhook = webhook;
		
			this.members = listMembers;
			this.subscribers_count = subscribers_count;
		}
		public ViberInfoDTO() {
			super();
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getStatus_message() {
			return status_message;
		}
		public void setStatus_message(String status_message) {
			this.status_message = status_message;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getChat_hostname() {
			return chat_hostname;
		}
		public void setChat_hostname(String chat_hostname) {
			this.chat_hostname = chat_hostname;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getUri() {
			return uri;
		}
		public void setUri(String uri) {
			this.uri = uri;
		}
		public String getIcon() {
			return icon;
		}
		public void setIcon(String icon) {
			this.icon = icon;
		}
		public String getCategory() {
			return category;
		}
		public void setCategory(String category) {
			this.category = category;
		}
		public String getSubcategory() {
			return subcategory;
		}
		public void setSubcategory(String subcategory) {
			this.subcategory = subcategory;
		}
		public LocationDTO getLocation() {
			return location;
		}
		public void setLocation(LocationDTO location) {
			this.location = location;
		}
		public String getCountry() {
			return country;
		}
		public void setCountry(String country) {
			this.country = country;
		}
		public String getWebhook() {
			return webhook;
		}
		public void setWebhook(String webhook) {
			this.webhook = webhook;
		}

		public List<MembersDTO> getListMembers() {
			return members;
		}
		public void setListMembers(List<MembersDTO> listMembers) {
			this.members = members;
		}
		public String getSubscribers_count() {
			return subscribers_count;
		}
		public void setSubscribers_count(String subscribers_count) {
			this.subscribers_count = subscribers_count;
		}
		
		
		
}
