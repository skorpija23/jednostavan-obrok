package com.jednostavan_obrok.dto;

public class ViberUserDTO  implements DTOEntity{
	
	private String id;
	
	private String viberId;
	
	private String name;
	
	private boolean active;

	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ViberUserDTO() {
		super();
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getViberId() {
		return viberId;
	}
	public void setViberId(String viberId) {
		this.viberId = viberId;
	}

}
