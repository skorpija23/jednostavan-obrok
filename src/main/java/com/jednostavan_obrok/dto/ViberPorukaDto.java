package com.jednostavan_obrok.dto;

public class ViberPorukaDto {
    private  String viberPoruka;

    public ViberPorukaDto(String viberPoruka) {
        this.viberPoruka = viberPoruka;
    }

    public ViberPorukaDto() {
    }

    public String getViberPoruka() {
        return viberPoruka;
    }

    public void setViberPoruka(String viberPoruka) {
        this.viberPoruka = viberPoruka;
    }
}
