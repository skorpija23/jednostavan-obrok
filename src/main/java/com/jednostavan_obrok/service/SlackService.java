package com.jednostavan_obrok.service;

import com.jednostavan_obrok.dto.PorukaDTO;

public interface SlackService {
    void sendMessageToSlack(PorukaDTO poruka);
}
