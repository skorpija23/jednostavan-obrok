package com.jednostavan_obrok.dto;

public class PorudzbinaDto {
    private int idObrok;
    private int kolicina;

    public PorudzbinaDto(int idObrok, int kolicina) {
        this.idObrok = idObrok;
        this.kolicina = kolicina;
    }

    public PorudzbinaDto() {
    }

    public int getIdObrok() {
        return idObrok;
    }

    public void setIdObrok(int idObrok) {
        this.idObrok = idObrok;
    }

    public int getKolicina() {
        return kolicina;
    }

    public void setKolicina(int kolicina) {
        this.kolicina = kolicina;
    }
}
