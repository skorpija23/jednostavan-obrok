package com.jednostavan_obrok.service;



import com.jednostavan_obrok.model.Korisnik;

import java.util.List;

public interface ViberMessagesService {


	void sendMessageToAll(String message);


	List<Korisnik> findAll();

	void save(String viberUserDto);
}
