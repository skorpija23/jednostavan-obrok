package com.jednostavan_obrok.dto;

public class ResetPasswordDto {
    private String password;
    private String confirm_password;

    public ResetPasswordDto(String password, String confirm_password) {
        this.password = password;
        this.confirm_password = confirm_password;
    }

    public ResetPasswordDto() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirm_password() {
        return confirm_password;
    }

    public void setConfirm_password(String confirm_password) {
        this.confirm_password = confirm_password;
    }
}
