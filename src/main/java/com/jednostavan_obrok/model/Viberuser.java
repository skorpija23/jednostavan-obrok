package com.jednostavan_obrok.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
@Entity
@Getter
@Setter
@NamedQuery(name="Viberuser.findAll", query="SELECT v FROM Viberuser v")
public class Viberuser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int id;

	private String viberId;

	private String viberName;
	
	private String viberRole;

	public Viberuser() {
	}

	public Viberuser(int id, String viberId, String viberName, String viberRole) {
		this.id = id;
		this.viberId = viberId;
		this.viberName = viberName;
		this.viberRole = viberRole;
	}
}
