package com.jednostavan_obrok.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tip_obroka database table.
 * 
 */
@Entity
@Table(name="tip_obroka")
@NamedQuery(name="TipObroka.findAll", query="SELECT t FROM TipObroka t")
public class TipObroka implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_tip_obroka")
	private int idTipObroka;

	private String naziv;

	//bi-directional many-to-one association to Obrok
	@OneToMany(mappedBy="tipObroka")
	private List<Obrok> obroks;

	public TipObroka() {
	}

	public int getIdTipObroka() {
		return this.idTipObroka;
	}

	public void setIdTipObroka(int idTipObroka) {
		this.idTipObroka = idTipObroka;
	}

	public String getNaziv() {
		return this.naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public List<Obrok> getObroks() {
		return this.obroks;
	}

	public void setObroks(List<Obrok> obroks) {
		this.obroks = obroks;
	}

	public Obrok addObrok(Obrok obrok) {
		getObroks().add(obrok);
		obrok.setTipObroka(this);

		return obrok;
	}

	public Obrok removeObrok(Obrok obrok) {
		getObroks().remove(obrok);
		obrok.setTipObroka(null);

		return obrok;
	}

}