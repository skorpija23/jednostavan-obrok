package com.jednostavan_obrok.controller;


import com.jednostavan_obrok.dto.PorukaDTO;
import com.jednostavan_obrok.service.SlackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/slack")
public class SlackController {
    @Autowired
    private SlackService slackService;

    @PostMapping("/poruka")
    public ResponseEntity<PorukaDTO> sendSimpleMessageToSlack(@RequestBody PorukaDTO poruka) {
       System.out.println("Upao u slack Controller!!!!");

        slackService.sendMessageToSlack(poruka);
        return ResponseEntity.ok(poruka);
    }
}
