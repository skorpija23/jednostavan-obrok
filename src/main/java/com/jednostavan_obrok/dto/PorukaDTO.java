package com.jednostavan_obrok.dto;

public class PorukaDTO {
    private String poruka;

    public PorukaDTO(String poruka) {
        this.poruka = poruka;
    }

    public PorukaDTO() {
    }

    public String getPoruka() {
        return poruka;
    }

    public void setPoruka(String poruka) {
        this.poruka = poruka;
    }
}
