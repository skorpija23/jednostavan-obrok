package com.jednostavan_obrok.dto;

import java.util.Date;

public class PorudzbinaStavkaDto {

    int idPorudzbine;
    Date vreme;
    int kolicina;
    String tipObroka;
    String nazivObroka;
    String velicina;
    Double cena;

    public PorudzbinaStavkaDto(int idPorudzbine, Date vreme, int kolicina, String tipObroka, String nazivObroka, String velicina, Double cena) {
        this.idPorudzbine = idPorudzbine;
        this.vreme = vreme;
        this.kolicina = kolicina;
        this.tipObroka = tipObroka;
        this.nazivObroka = nazivObroka;
        this.velicina = velicina;
        this.cena = cena;
    }

    public PorudzbinaStavkaDto() {
    }

    public int getIdPorudzbine() {
        return idPorudzbine;
    }

    public void setIdPorudzbine(int idPorudzbine) {
        this.idPorudzbine = idPorudzbine;
    }

    public Date getVreme() {
        return vreme;
    }

    public void setVreme(Date vreme) {
        this.vreme = vreme;
    }

    public int getKolicina() {
        return kolicina;
    }

    public void setKolicina(int kolicina) {
        this.kolicina = kolicina;
    }

    public String getTipObroka() {
        return tipObroka;
    }

    public void setTipObroka(String tipObroka) {
        this.tipObroka = tipObroka;
    }

    public String getNazivObroka() {
        return nazivObroka;
    }

    public void setNazivObroka(String nazivObroka) {
        this.nazivObroka = nazivObroka;
    }

    public String getVelicina() {
        return velicina;
    }

    public void setVelicina(String velicina) {
        this.velicina = velicina;
    }

    public Double getCena() {
        return cena;
    }

    public void setCena(Double cena) {
        this.cena = cena;
    }
}
