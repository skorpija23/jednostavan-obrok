package com.jednostavan_obrok.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the stavka database table.
 * 
 */
@Entity
@NamedQuery(name="Stavka.findAll", query="SELECT s FROM Stavka s")
public class Stavka implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_stavka")
	private int idStavka;

	private int kolicina;

	//bi-directional many-to-one association to Porudzbina
	@ManyToOne
    @JoinColumn(name="porudzbina_id_porudzbina")
	private Porudzbina porudzbina;

	//bi-directional many-to-one association to Obrok
	@ManyToOne
	@JoinColumn(name="obrok_id_obrok")
	private Obrok obrok;


	public Stavka() {
	}


	public int getIdStavka() {
		return this.idStavka;
	}

	public void setIdStavka(int idStavka) {
		this.idStavka = idStavka;
	}

	public int getKolicina() {
		return this.kolicina;
	}

	public void setKolicina(int kolicina) {
		this.kolicina = kolicina;
	}

	public Obrok getObrok() {
		return this.obrok;
	}

	public void setObrok(Obrok obrok) {
		this.obrok = obrok;
	}

	public Porudzbina getPorudzbina() {
		return porudzbina;
	}

	public void setPorudzbina(Porudzbina porudzbina) {
		this.porudzbina = porudzbina;
	}
}