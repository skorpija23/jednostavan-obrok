package com.jednostavan_obrok.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the porudzbina database table.
 * 
 */
@Entity
@NamedQuery(name="Porudzbina.findAll", query="SELECT p FROM Porudzbina p")
public class Porudzbina implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_porudzbina")
	private int idPorudzbina;


	private byte otkazivanje;


	@Temporal(TemporalType.TIMESTAMP)
	private Date vreme;

	//bi-directional many-to-one association to Stavka
	@OneToMany(mappedBy="porudzbina")
	private List<Stavka> stavkas;

	//bi-directional many-to-one association to Korisnik
	@ManyToOne
	@JoinColumn(name="korisnik_id_korisnik")
	private Korisnik korisnik;



	public Porudzbina() {
	}

	public int getIdPorudzbina() {
		return this.idPorudzbina;
	}

	public void setIdPorudzbina(int idPorudzbina) {
		this.idPorudzbina = idPorudzbina;
	}

	public byte getOtkazivanje() {
		return this.otkazivanje;
	}

	public void setOtkazivanje(byte otkazivanje) {
		this.otkazivanje = otkazivanje;
	}


	public Date getVreme() {
		return this.vreme;
	}

	public void setVreme(Date vreme) {
		this.vreme = vreme;
	}



	public Korisnik getKorisnik() {
		return this.korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}



}