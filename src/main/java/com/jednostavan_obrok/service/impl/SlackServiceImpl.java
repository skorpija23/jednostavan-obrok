package com.jednostavan_obrok.service.impl;

import com.jednostavan_obrok.dto.PorukaDTO;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.jednostavan_obrok.service.SlackService;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class SlackServiceImpl implements SlackService {


    @Autowired
    private OkHttpClient client;
    @Override
    public void sendMessageToSlack(PorukaDTO message) {
        System.out.println("Upao u send message, slanje SLack poruke ");

        JSONObject jsonSlackBody = new JSONObject();
        try {
            jsonSlackBody.put( "username", "Milan Nedeljkovic");
            jsonSlackBody.put( "channel", "#jednostavan-obrok");
            jsonSlackBody.put( "text", message.getPoruka());

        } catch (Exception e) {
            e.printStackTrace();
        }
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        @SuppressWarnings("deprecation")
        RequestBody body = RequestBody.create(JSON, jsonSlackBody.toString());


//        System.out.println(body.toString()+" body message");

        Request requested = new Request.Builder()
                .url("https://hooks.slack.com/services/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")
                .post(body)
                .build();
        System.out.println(requested);


        try {
            Response response = client.newCall(requested).execute();
            String resStr = response.body().string();
            System.out.println(resStr);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

//    @Value("${slack.webhook}")
//    private String urlSlackWebHook;
//    public void sendMessageToSlack(String message){
//        StringBuilder messageBuilder=new StringBuilder();
//        messageBuilder.append("*Moja poruka: "+message+NEW_LINE);
//        process(messageBuilder.toString());
//    }
//    private void process(String message){
//        Playload playload=Playload.builder()
//                .channel("#app-alerts")
//                .username("Milan Nedeljković")
//                .iconEmoji(":rocket:")
//                .text(message)
//                .build();
//        try{
//            WebhookResponse webhookResponse= Slack.getInstance().send(
//                    urlSlackWebHook, playload);
////            LOGGER.info("code -> "+webhookResponse.getCode());
////            LOGGER.info("body -> "+webhookResponse.getBody);
//        }catch (IOException e){
//            LOGGER.error("Neocekivana greska, Webhook: "+urlSlackWebHook);
//        }
//    }

}
