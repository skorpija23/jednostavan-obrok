package com.jednostavan_obrok.viber;//package com.project.auto.viber;
//
//import com.google.common.base.Charsets;
//import com.google.common.base.Preconditions;
//import com.google.common.io.CharStreams;
//import com.google.common.util.concurrent.Futures;
//import com.viber.bot.Request;
//import com.viber.bot.ViberSignatureValidator;
//import com.viber.bot.api.ViberBot;
//import com.viber.bot.message.TextMessage;
//import com.viber.bot.profile.BotProfile;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.boot.context.event.ApplicationReadyEvent;
//import org.springframework.context.ApplicationListener;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestHeader;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.annotation.Nullable;
//import javax.inject.Inject;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.util.Optional;
//import java.util.concurrent.ExecutionException;
//
//
//
// 
//@RestController
//@SpringBootApplication
//public class ViberMessage implements ApplicationListener<ApplicationReadyEvent> {
//
//    @Inject
//    private ViberBot bot;
//
//    @Inject
//    private ViberSignatureValidator signatureValidator;
//
//    @Value("${application.viber-bot.webhook-url}")
//    private String webhookUrl;
//
//	private final String url= "https://chatapi.viber.com/pa/set_webhook";
//	
//	private final String body= "{\r\n" + 
//			"   \"url\":\"https://06d0f9fe5956.ngrok.io/app/prodavnicaDelova\",\r\n" + 
//			"   \"event_types\":[\r\n" + 
//			"      \"delivered\",\r\n" + 
//			"      \"seen\",\r\n" + 
//			"      \"failed\",\r\n" + 
//			"      \"subscribed\",\r\n" + 
//			"      \"unsubscribed\",\r\n" + 
//			"      \"conversation_started\"\r\n" + 
//			"   ],\r\n" + 
//			"   \"send_name\": true,\r\n" + 
//			"   \"send_photo\": true\r\n" + 
//			"}";
//
//    public static void main(String[] args) {
//        SpringApplication.run(ViberBot.class, args);
//       
//    }
//
//    @Override
//    public void onApplicationEvent(ApplicationReadyEvent appReadyEvent) {
//        try {
//        	System.out.println("-----> " + webhookUrl);
//           
//        	
//        	bot.setWebhook(webhookUrl).get();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        bot.onMessageReceived((event, message, response) -> response.send(message)); // echos everything back
//        bot.onConversationStarted(event -> Futures.immediateFuture(Optional.of( // send 'Hi UserName' when conversation is started
//                new TextMessage("Hi " + event.getUser().getName()))));
//    }
//
//    @PostMapping(value = "/viber", produces = "application/json")
//    public String incoming(@RequestBody String json,
//                           @RequestHeader("X-Viber-Content-Signature") String serverSideSignature)
//            throws ExecutionException, InterruptedException, IOException {
//    	System.out.println(json);
//        Preconditions.checkState(signatureValidator.isSignatureValid(serverSideSignature, json), "invalid signature");
//        @Nullable InputStream response = bot.incoming(Request.fromJsonString(json)).get();
//        return response != null ? CharStreams.toString(new InputStreamReader(response, Charsets.UTF_16)) : null;
//    }
//
//    @Configuration
//    public class BotConfiguration {
//
//        @Value("${application.viber-bot.auth-token}")
//        private String authToken;
//
//        @Value("${application.viber-bot.name}")
//        private String name;
//
//        @Nullable
//        @Value("${application.viber-bot.avatar:@null}")
//        private String avatar;
//
//        @Bean
//        ViberBot viberBot() {
//            return new ViberBot(new BotProfile(name, avatar), authToken);
//        }
//
//        @Bean
//        ViberSignatureValidator signatureValidator() {
//            return new ViberSignatureValidator(authToken);
//        }
//    }
//}