package com.jednostavan_obrok.service;


import com.jednostavan_obrok.dto.KorisnikDTO;
import com.jednostavan_obrok.dto.KorisnikIzmenaDto;
import com.jednostavan_obrok.dto.LogovanjeKorisnikaDTO;
import com.jednostavan_obrok.dto.RegistracijaKorisnikaDTO;
import com.jednostavan_obrok.model.Korisnik;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;

public interface UserService extends UserDetailsService {

    Object logovanjeKorisnika(LogovanjeKorisnikaDTO request) throws Exception;

    Korisnik findByUsername(String username);

    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;

    Korisnik registracijaKupca(RegistracijaKorisnikaDTO request) throws Exception;


    List<KorisnikDTO> korisnici();

    Boolean brisanjeKorisnika(int idKorisnik);

    void deaktiviranjeKorisnika(int idKorisnik);


    Korisnik findUserByEmail(String mail);

    void createPasswordResetTokenForUser(Korisnik korisnik, String token);

    Boolean resetovanjeLozinke(String token, String password);

    KorisnikIzmenaDto podaciZaIzmenu(String token);

    void promenaKorisnika(KorisnikIzmenaDto request);
}
