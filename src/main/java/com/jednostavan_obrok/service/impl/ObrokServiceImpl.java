package com.jednostavan_obrok.service.impl;

import com.jednostavan_obrok.dto.KompletnaPorudzbinaDto;
import com.jednostavan_obrok.dto.ObrokDTO;
import com.jednostavan_obrok.dto.PorudzbinaDto;
import com.jednostavan_obrok.dto.PorudzbinaStavkaDto;
import com.jednostavan_obrok.model.*;
import com.jednostavan_obrok.repository.*;
import com.jednostavan_obrok.security.SecurityConfiguration;
import com.jednostavan_obrok.security.TokenUtils;
import com.jednostavan_obrok.service.ObrokService;
import com.jednostavan_obrok.service.ViberMessagesService;
import com.jednostavan_obrok.telegram.telegramMessagesSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ObrokServiceImpl implements ObrokService {
    @Autowired
    private SecurityConfiguration configuration;
    @Autowired
    private TokenUtils tokenUtils;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private KorisnikRepository korisnikRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private ObrokRepository obrokRepository;
    @Autowired
    private TipObrokaRepository tipObrokaRepository;
    @Autowired
    private PorudzbinaRepository porudzbinaRepository;
    @Autowired
    private StavkaRepository stavkaRepository;
    @Autowired
    private ViberMessagesService viberService;

    @Override
    @Transactional
    public Obrok dodavanjeObroka(ObrokDTO request) throws ParseException {
       Obrok obrok=new Obrok();
      String tipObrokaNaziv=request.getTipObroka();
        TipObroka tipObroka=new TipObroka();
      if(tipObrokaNaziv!=null && !tipObrokaNaziv.isEmpty()){
          tipObroka.setNaziv(tipObrokaNaziv);
          tipObrokaRepository.saveAndFlush(tipObroka);
      }
       if(request!=null){
           obrok.setNaziv(request.getNaziv());
           obrok.setCena(Double.parseDouble(request.getCena()));
           obrok.setOpis(request.getOpis());
           String dateStr = request.getDatum();
           SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
           Date date = formatter.parse(dateStr);
           obrok.setDatum(date);
           obrok.setVelicina(request.getVelicina());
           obrok.setTipObroka(tipObroka);
           obrok.setSlika(request.getSlika());
           obrokRepository.saveAndFlush(obrok);
       }
        return obrok;
    }

    @Override
    public List<ObrokDTO> dobaviObroke() {
       List<Obrok> listaObroka=obrokRepository.findAll();
       List<ObrokDTO> listaObrokaDto=new ArrayList<ObrokDTO>();
       for(Obrok obrok: listaObroka){
           ObrokDTO obrokDTO=new ObrokDTO();
           obrokDTO.setCena(String.valueOf(obrok.getCena()));
           obrokDTO.setNaziv(obrok.getNaziv());
           DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
           String strDate = dateFormat.format(obrok.getDatum());
           obrokDTO.setDatum(strDate);
           obrokDTO.setOpis(obrok.getOpis());
           obrokDTO.setVelicina(obrok.getVelicina());
//           TipObroka tipObroka=tipObrokaRepository.findById(obrok.getIdObrok()).get();
//           obrokDTO.setTipObroka(tipObroka.getNaziv());
           obrokDTO.setIdObrok(obrok.getIdObrok());
           obrokDTO.setSlika(obrok.getSlika());
           listaObrokaDto.add(obrokDTO);
       }
        return listaObrokaDto;
    }

    @Override
    public List<PorudzbinaStavkaDto> pronadjiSvePorudzbineKOrisnika(int idKorisnik) throws ParseException{
       List<Object[]> listaObjekata=porudzbinaRepository.findAllPorudzbinaByIdKorisnik(idKorisnik);
       List<PorudzbinaStavkaDto> porudzbinaStavkaDtoList=new ArrayList<PorudzbinaStavkaDto>();
       for(Object[] ob: listaObjekata) {
           PorudzbinaStavkaDto porudzbinaStavkaDto = new PorudzbinaStavkaDto();
           porudzbinaStavkaDto.setIdPorudzbine(Integer.parseInt(ob[0].toString()));
           porudzbinaStavkaDto.setNazivObroka(ob[1].toString());
           porudzbinaStavkaDto.setVelicina(ob[2].toString());
           porudzbinaStavkaDto.setCena(Double.parseDouble(ob[3].toString()));
           SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
           String sDateOne = ob[5].toString();
           Date datum = formatter.parse(sDateOne);
           porudzbinaStavkaDto.setVreme(datum);
           porudzbinaStavkaDto.setKolicina(Integer.parseInt(ob[6].toString()));
           porudzbinaStavkaDto.setTipObroka(ob[7].toString());
           porudzbinaStavkaDtoList.add(porudzbinaStavkaDto);
       }

        return porudzbinaStavkaDtoList;
    }

    @Override
    public List<KompletnaPorudzbinaDto> pravljenjePorudzbine(List<PorudzbinaDto> request, String token) {

      Map<Integer, KompletnaPorudzbinaDto> mapaPorudzbina=new HashMap<Integer, KompletnaPorudzbinaDto>();
        for(PorudzbinaDto po: request){
          KompletnaPorudzbinaDto kompletnaPorudzbinaDto=new KompletnaPorudzbinaDto();
          int idObroka=po.getIdObrok();
          Obrok obrok=obrokRepository.findById(idObroka).get();
          kompletnaPorudzbinaDto.setNaziv(obrok.getNaziv());
          kompletnaPorudzbinaDto.setIdObroka(obrok.getIdObrok());
          kompletnaPorudzbinaDto.setVelicina(obrok.getVelicina());
          kompletnaPorudzbinaDto.setCena(obrok.getCena());

          if(mapaPorudzbina.containsKey(idObroka)){
             for(Map.Entry<Integer, KompletnaPorudzbinaDto> map: mapaPorudzbina.entrySet()) {

                 int kolicina = po.getKolicina()+map.getValue().getKolicina();
                 double cenaPutaKolicina = obrok.getCena() * kolicina;
                 kompletnaPorudzbinaDto.setCenaPutaKolicina(cenaPutaKolicina);
                 kompletnaPorudzbinaDto.setKolicina(kolicina);
                 mapaPorudzbina.put(idObroka, kompletnaPorudzbinaDto);
             }
          }else{

                  double cenaPutaKolicina=obrok.getCena()*po.getKolicina();
                  kompletnaPorudzbinaDto.setCenaPutaKolicina(cenaPutaKolicina);
                  kompletnaPorudzbinaDto.setKolicina(po.getKolicina()) ;
                  mapaPorudzbina.put(idObroka, kompletnaPorudzbinaDto);
          }

      }



        return mapaPorudzbina.values().stream().collect(Collectors.toList());
    }

    @Override
    @Transactional
    public String kreiranjePorudzbine(List<PorudzbinaDto> listaRequesta, String token) throws ParseException {
        String username = tokenUtils.getUsernameFromToken(token.split("\\s")[1]);
        Korisnik korisnik=korisnikRepository.findByUsername(username);
        System.out.println("Korisnik iz kreiranje porudzbine "+korisnik);
        Porudzbina porudzbina=new Porudzbina();
        porudzbina.setKorisnik(korisnik);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date currentDate = new Date();
        porudzbina.setVreme(currentDate);
        porudzbinaRepository.saveAndFlush(porudzbina);
       StringBuilder sb=new StringBuilder();
       sb.append("*Kreirana je porudzbina broj ->* *"+porudzbina.getIdPorudzbina()+"*\r\n");
       sb.append("*vreme porudzbine ->* "+porudzbina.getVreme()+"\r\n");
       sb.append("*stavke su ->*\r\n");
       double ukupnaCena=0;
        for(PorudzbinaDto po: listaRequesta) {
            Stavka stavka = new Stavka();
            Obrok obrok = obrokRepository.findById(po.getIdObrok()).get();
            ukupnaCena+=obrok.getCena()*po.getKolicina();
            stavka.setObrok(obrok);
            stavka.setKolicina(po.getKolicina());
            stavka.setPorudzbina(porudzbina);
            stavkaRepository.saveAndFlush(stavka);
            sb.append("*naziv ->*"+"```"+obrok.getNaziv()+"```\r\n");
            sb.append("*cena ->*"+"```"+obrok.getCena()+"``` ```dinara```\r\n");
            sb.append("*kolicina ->*"+"```"+po.getKolicina()+"```\r\n");
            sb.append("*velicina ->* "+"```"+obrok.getVelicina()+"``` \r\n");
        }
        sb.append("*UKUPNA IZNOS RACUNA =>* *"+ukupnaCena+"* *dinara* \r\n");
        sb.append("\r\n*PRIJATNO !!!* :)");
        String poruka=sb.toString();
        viberService.sendMessageToAll(poruka);
        telegramMessagesSender.sendToTelegram(poruka);

        return "Kreirali ste porudzbinu !";
    }

    @Override
    public Boolean brisanjeObroka(int idObroka) {
       Obrok obrokZaBrisanje=obrokRepository.findById(idObroka).get();
       obrokRepository.delete(obrokZaBrisanje);
        return true;
    }
}
