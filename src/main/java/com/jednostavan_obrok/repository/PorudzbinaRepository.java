package com.jednostavan_obrok.repository;

import com.jednostavan_obrok.model.Porudzbina;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PorudzbinaRepository extends JpaRepository<Porudzbina,Integer> {

    @Query(value = " Select p.id_porudzbina, o.naziv, o.velicina, o.cena, o.datum ,p.vreme, s.kolicina, tp.naziv as tip_obroka from  korisnik as k inner join porudzbina as p on k.id_korisnik = p.korisnik_id_korisnik\n" +
            " inner join stavka as s on p.id_porudzbina = s.porudzbina_id_porudzbina\n" +
            " inner join obrok as o on s.obrok_id_obrok=o.id_obrok\n" +
            " inner join tip_obroka as tp on o.tip_obroka_id_tip_obroka = tp.id_tip_obroka\n" +
            " where k.id_korisnik = (?1) group by p.id_porudzbina", nativeQuery = true)
    List<Object[]> findAllPorudzbinaByIdKorisnik(int idKorisnik);
}
