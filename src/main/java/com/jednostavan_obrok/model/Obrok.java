package com.jednostavan_obrok.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the obrok database table.
 * 
 */
@Entity
@NamedQuery(name="Obrok.findAll", query="SELECT o FROM Obrok o")
public class Obrok implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_obrok")
	private int idObrok;

	private double cena;

	@Temporal(TemporalType.DATE)
	private Date datum;

	private String opis;

	private String velicina;

	private String slika;

	@Column(name="naziv")
	private String naziv;

	//bi-directional many-to-one association to TipObroka
	@ManyToOne
	@JoinColumn(name="tip_obroka_id_tip_obroka")
	private TipObroka tipObroka;

	//bi-directional many-to-one association to Stavka
	@OneToMany(mappedBy="obrok")
	private List<Stavka> stavkas;

	public Obrok() {
	}

	public int getIdObrok() {
		return this.idObrok;
	}

	public void setIdObrok(int idObrok) {
		this.idObrok = idObrok;
	}

	public double getCena() {
		return this.cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public Date getDatum() {
		return this.datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public String getOpis() {
		return this.opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getVelicina() {
		return this.velicina;
	}

	public void setVelicina(String velicina) {
		this.velicina = velicina;
	}

	public String getNaziv() {
		return this.naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public TipObroka getTipObroka() {
		return this.tipObroka;
	}

	public void setTipObroka(TipObroka tipObroka) {
		this.tipObroka = tipObroka;
	}

	public List<Stavka> getStavkas() {
		return this.stavkas;
	}

	public void setStavkas(List<Stavka> stavkas) {
		this.stavkas = stavkas;
	}

	public Stavka addStavka(Stavka stavka) {
		getStavkas().add(stavka);
		stavka.setObrok(this);

		return stavka;
	}

	public Stavka removeStavka(Stavka stavka) {
		getStavkas().remove(stavka);
		stavka.setObrok(null);

		return stavka;
	}

	public String getSlika() {
		return slika;
	}

	public void setSlika(String slika) {
		this.slika = slika;
	}
}