package com.jednostavan_obrok.repository;

import com.jednostavan_obrok.model.Passwordresettoken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PasswordresettokenRepository extends JpaRepository<Passwordresettoken,Integer> {

    @Query(value = " Select p.korisnik_id_korisnik from passwordresettoken as p where p.token like (?1) " ,
            nativeQuery = true)
    Object findByToken(String token);
}
