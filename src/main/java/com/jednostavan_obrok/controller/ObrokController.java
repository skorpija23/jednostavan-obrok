package com.jednostavan_obrok.controller;

import com.jednostavan_obrok.dto.*;
import com.jednostavan_obrok.model.Obrok;
import com.jednostavan_obrok.security.TokenUtils;
import com.jednostavan_obrok.service.ObrokService;
import com.jednostavan_obrok.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


@RestController
    @RequestMapping("/obrok")
    public class ObrokController {

        @Autowired
        private UserService userService;

        @Autowired
        private ObrokService obrokService;

        @Autowired
        private AuthenticationManager authenticationManager;

        @Qualifier("userServiceImpl")
        @Autowired
        private UserDetailsService userDetailsService;

        @Autowired
        private TokenUtils tokenUtils;

        Logger logger = LoggerFactory.getLogger(ObrokController.class);


    @PostMapping("/dodavanjeObroka")
    public ResponseEntity<?> dodavanjeArtikla(@RequestBody ObrokDTO request ) {
        try {
           Obrok obrok= obrokService.dodavanjeObroka(request);
            PorukaDTO poruka=new PorukaDTO("Uspesno dodat obrok: "+obrok.toString());
            return new ResponseEntity<>(poruka, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(new ErrorDTO("Neuspesna registracija!"), HttpStatus.BAD_REQUEST);
        }

    }
    @GetMapping("dobaviObroke")
    public ResponseEntity<?> dobaviArtikle() {
        logger.info("Index page loaded");
        List<ObrokDTO> response = null;
        try {
            response = obrokService.dobaviObroke();
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(new ErrorDTO(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }
    @GetMapping("/listaPorudzbina")
    public ResponseEntity<List<?>> listaAkcija(@RequestParam("idKorisnik") int idKorisnik) throws ParseException {
System.out.println("Id korisnika "+idKorisnik);
        return ResponseEntity.ok().body(
                obrokService.pronadjiSvePorudzbineKOrisnika(idKorisnik)
        );
    }
    @PostMapping("porudzbina")
    public ResponseEntity<?> porudzbina(@RequestBody List<PorudzbinaDto> listaRequesta, @RequestHeader("Authorization") String token){

            List<KompletnaPorudzbinaDto> listaPorudzbine= obrokService.pravljenjePorudzbine(listaRequesta,token);
            System.out.println(listaPorudzbine.toString());
            return ResponseEntity.ok().body(
                    listaPorudzbine);

    }
    @PostMapping("kreiranjePorudzbine")
    public ResponseEntity<?> kreiranjePorudzbine(@RequestBody List<PorudzbinaDto> listaRequesta, @RequestHeader("Authorization") String token ) {

        try {

            String porukaServisa= obrokService.kreiranjePorudzbine(listaRequesta, token);
            System.out.println(" <-- Poruka iz servisa -->  "+porukaServisa);
            PorukaDTO poruka=new PorukaDTO(porukaServisa);
            return new ResponseEntity<>(poruka, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(new ErrorDTO("Neuspesna kreiranje porudzbine!"), HttpStatus.BAD_REQUEST);
        }
    }
    @DeleteMapping("brisanjeObroka")
    public ResponseEntity<?> brisanjeObroka(@RequestParam("idObroka") int idObroka ){
        try {
            System.out.println("@@@@@@@@ KOntroler === Id Obroka za brisanje "+idObroka);
            Boolean izbrisano=obrokService.brisanjeObroka(idObroka);
            PorukaDTO poruka=new PorukaDTO();
            if(izbrisano){
                poruka=new PorukaDTO("Obrok je uspesno obrisan!");
            }
            return new ResponseEntity<>(poruka, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(new ErrorDTO("Neuspelo brisanje!"), HttpStatus.BAD_REQUEST);
        }
    }
}
