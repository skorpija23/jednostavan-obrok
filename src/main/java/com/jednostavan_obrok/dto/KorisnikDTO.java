package com.jednostavan_obrok.dto;
public class KorisnikDTO {
  private int idKorisnik;
   private String ime;
    private String prezime;
    private String adresa;
    private String mesto;
    private int mobilni;
    private String email;
    private byte deaktiviranje;

    public KorisnikDTO(int idKorisnik, String ime, String prezime, String adresa, String mesto, int mobilni, String email, byte deaktiviranje) {
        this.idKorisnik = idKorisnik;
        this.ime = ime;
        this.prezime = prezime;
        this.adresa = adresa;
        this.mesto = mesto;
        this.mobilni = mobilni;
        this.email = email;
        this.deaktiviranje = deaktiviranje;
    }

    public KorisnikDTO() {
    }

    public int getIdKorisnik() {
        return idKorisnik;
    }

    public void setIdKorisnik(int idKorisnik) {
        this.idKorisnik = idKorisnik;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getMesto() {
        return mesto;
    }

    public void setMesto(String mesto) {
        this.mesto = mesto;
    }

    public int getMobilni() {
        return mobilni;
    }

    public void setMobilni(int mobilni) {
        this.mobilni = mobilni;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public byte getDeaktiviranje() {
        return deaktiviranje;
    }

    public void setDeaktiviranje(byte deaktiviranje) {
        this.deaktiviranje = deaktiviranje;
    }

    public void setMobilni(String valueOf) {
    }
}
