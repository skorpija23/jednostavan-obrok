package com.jednostavan_obrok.service;

import com.jednostavan_obrok.dto.KompletnaPorudzbinaDto;
import com.jednostavan_obrok.dto.ObrokDTO;
import com.jednostavan_obrok.dto.PorudzbinaDto;
import com.jednostavan_obrok.dto.PorudzbinaStavkaDto;
import com.jednostavan_obrok.model.Obrok;

import java.text.ParseException;
import java.util.List;

public interface ObrokService {
    Obrok dodavanjeObroka(ObrokDTO request) throws ParseException;

    List<ObrokDTO> dobaviObroke();

    List<PorudzbinaStavkaDto> pronadjiSvePorudzbineKOrisnika(int idKorisnik) throws ParseException;

    List<KompletnaPorudzbinaDto> pravljenjePorudzbine(List<PorudzbinaDto> request, String token);

    String kreiranjePorudzbine(List<PorudzbinaDto> listaRequesta, String token) throws ParseException;

    Boolean brisanjeObroka(int idObroka);
}
