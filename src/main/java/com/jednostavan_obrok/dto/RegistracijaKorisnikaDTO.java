package com.jednostavan_obrok.dto;

public class RegistracijaKorisnikaDTO {

    private String ime;
    private String prezime;
    private String password;
    private String username;
    private String adresa;
    private String email;
    private String mobilni;
    private String mesto;

    public RegistracijaKorisnikaDTO(String ime, String prezime, String password, String username, String adresa, String email, String mobilni, String mesto) {
        this.ime = ime;
        this.prezime = prezime;
        this.password = password;
        this.username = username;
        this.adresa = adresa;
        this.email = email;
        this.mobilni = mobilni;
        this.mesto = mesto;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobilni() {
        return mobilni;
    }

    public void setMobilni(String mobilni) {
        this.mobilni = mobilni;
    }

    public String getMesto() {
        return mesto;
    }

    public void setMesto(String mesto) {
        this.mesto = mesto;
    }
}
