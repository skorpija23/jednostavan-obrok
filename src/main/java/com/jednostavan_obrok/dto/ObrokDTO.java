package com.jednostavan_obrok.dto;

import com.jednostavan_obrok.model.TipObroka;

import java.util.Date;

public class ObrokDTO {

    private int idObrok;

    private String cena;

    private String datum;

    private String opis;

    private String velicina;

    private String naziv;

    private String tipObroka;

    private String slika;

    public ObrokDTO(int idObrok, String cena, String datum, String opis, String velicina, String naziv, String tipObroka, String slika) {
        this.idObrok = idObrok;
        this.cena = cena;
        this.datum = datum;
        this.opis = opis;
        this.velicina = velicina;
        this.naziv = naziv;
        this.tipObroka = tipObroka;
        this.slika = slika;
    }

    public ObrokDTO() {
    }

    public String getCena() {
        return cena;
    }

    public void setCena(String cena) {
        this.cena = cena;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getVelicina() {
        return velicina;
    }

    public void setVelicina(String velicina) {
        this.velicina = velicina;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getTipObroka() {
        return tipObroka;
    }

    public void setTipObroka(String tipObroka) {
        this.tipObroka = tipObroka;
    }

    public String getSlika() {
        return slika;
    }

    public void setSlika(String slika) {
        this.slika = slika;
    }

    public int getIdObrok() {
        return idObrok;
    }

    public void setIdObrok(int idObrok) {
        this.idObrok = idObrok;
    }
}
