package com.jednostavan_obrok.repository;

import com.jednostavan_obrok.model.Stavka;
import com.jednostavan_obrok.model.TipObroka;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TipObrokaRepository extends JpaRepository<TipObroka,Integer> {
}
