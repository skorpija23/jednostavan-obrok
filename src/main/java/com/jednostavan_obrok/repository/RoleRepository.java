package com.jednostavan_obrok.repository;

import com.jednostavan_obrok.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role,Integer> {
}
