package com.jednostavan_obrok.repository;

import com.jednostavan_obrok.model.Viberuser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ViberUserRepository extends JpaRepository<Viberuser,Integer> {

    @Query(value = " Select k.id_korisnik from korisnik as k  where k.username like  (?1) order by k.id_korisnik desc limit 1" ,
            nativeQuery = true)
    Viberuser findByViberId(String viberId);
}
