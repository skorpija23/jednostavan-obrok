package com.jednostavan_obrok.controller;

import com.jednostavan_obrok.dto.*;
import com.jednostavan_obrok.email.emailSender;
import com.jednostavan_obrok.model.Korisnik;
import com.jednostavan_obrok.security.TokenUtils;
import com.jednostavan_obrok.service.UserService;

import com.jednostavan_obrok.service.ViberMessagesService;

import com.jednostavan_obrok.telegram.telegramMessagesSender;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;


    @Autowired
    private AuthenticationManager authenticationManager;

    @Qualifier("userServiceImpl")
    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private TokenUtils tokenUtils;
     @Autowired
    private ViberMessagesService viberService;

    @PostMapping("/registrujKorisnika")
    public ResponseEntity<?> registrujKorisnika(@RequestBody RegistracijaKorisnikaDTO request) throws Exception {
        try {
            Korisnik korisnik =userService.registracijaKupca(request);
            System.out.println("Upao u registraciju KORISNIKA"+korisnik);

            return new ResponseEntity<>(korisnik, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(new ErrorDTO("Neuspesna registracija!"), HttpStatus.BAD_REQUEST);
        }


    }

    @PostMapping("/logIn")
    public ResponseEntity<?> logovanjeKorisnika(@RequestBody LogovanjeKorisnikaDTO request) {
        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword());
            Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            UserDetails details = userDetailsService.loadUserByUsername(request.getUsername());
            Korisnik korisnikFromDB = userService.findByUsername(request.getUsername());

            LoggedInUserDTO loggedIn = new LoggedInUserDTO(korisnikFromDB.getIdKorisnik(), tokenUtils.generisiToken(details), korisnikFromDB.getUsername(), details.getAuthorities());
            if (korisnikFromDB.getDeaktiviranje() == (byte) 1) {
                return new ResponseEntity<>(new ErrorDTO("Vas nalog je deaktiviran!"), HttpStatus.UNAUTHORIZED);
            }
            return new ResponseEntity<>(loggedIn, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(new ErrorDTO("Logovanje nije uspelo!Pogresan unos!"), HttpStatus.BAD_REQUEST);
        }

    }
    @GetMapping("dobaviKorisnike")
            public ResponseEntity<?> dobaviKorisnike() {
        List<KorisnikDTO> korisnici = null;
        try {
            korisnici = userService.korisnici();
            return new ResponseEntity<>(korisnici, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(new ErrorDTO(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/setwebhook")
    public ResponseEntity<String> setWebhook() {
        return ResponseEntity.ok().body("ok");
    }

    @PostMapping(value = "/viberwebhook")
    public ResponseEntity<DTOEntity> getId(@RequestBody String viberUserDto) {
        System.out.println(viberUserDto);
        JSONParser parse = new JSONParser();
        JSONObject o;
        try {
            o = (JSONObject) parse.parse(viberUserDto);
            String event = (String) o.get("event");

            switch (event) {
                case "subscribed":
//                 return ResponseEntity.ok().body();
//				return new ResponseEntity<>(null, HttpStatus.CREATED);
                    viberService.save(viberUserDto);
                case "unsubscribed":
////				return new ResponseEntity<>(viberService.deleteFromDatabase(viberUserDto), HttpStatus.CREATED);
                case "conversation_started":
////				return new ResponseEntity<>(viberService.save(viberUserDto), HttpStatus.CREATED);
                case "message":
////				return new ResponseEntity<>(viberService.message(viberUserDto), HttpStatus.CREATED);
                default:
                    return null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
//	return new ResponseEntity<>(viberService.save(viberUserDto), HttpStatus.CREATED);
        return null;
    }

    @PostMapping("/sendmessage")
    public ResponseEntity<?> sendMessage(@RequestBody ViberPorukaDto request) {
     try{
        System.out.println("Upao u send message controller viber!");
        String viberPoruka=request.getViberPoruka();
        viberService.sendMessageToAll(viberPoruka);
         PorukaDTO poruka=new PorukaDTO("PorukaPoslata!");
         return new ResponseEntity<>(poruka, HttpStatus.OK);
     } catch (Exception e) {
         e.printStackTrace();
         return new ResponseEntity<>(new ErrorDTO("Neuspesna izmena!"), HttpStatus.BAD_REQUEST);

     }
    }

    @DeleteMapping("removeUser")
    public ResponseEntity<?> brisanjeArtikla(@RequestParam("id") int idKorisnik){
        try {
            Boolean izbrisano=userService.brisanjeKorisnika(idKorisnik);
            PorukaDTO poruka=new PorukaDTO();
            if(izbrisano){
                poruka=new PorukaDTO("Uspesno obrisana aktikal!");
            }
            return new ResponseEntity<>(poruka, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(new ErrorDTO("Neuspesna izmena!"), HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping("deaktiviranjeKorisnika")
    public ResponseEntity<?> blokiranjeKorisnika(@RequestParam("idKorisnik") int idKorisnik) {
        try {

            userService.deaktiviranjeKorisnika(idKorisnik);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(new ErrorDTO("Neuspesno deaktiviranje!"), HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping("forgotPassword")
    public ResponseEntity<?> zaboravljenaLozinka(@RequestBody ForgotPassDto request){
        try {
            Korisnik korisnik = userService.findUserByEmail(request.getMail());
            System.out.println("RESET KORISNIK "+korisnik.getUsername());
            StringBuilder sb=new StringBuilder();
            String token = UUID.randomUUID().toString();
            userService.createPasswordResetTokenForUser(korisnik, token);
            String linkZaObnovuLozinke="http://localhost:8080/resetLozinke.html?token=";
            sb.append(linkZaObnovuLozinke);
            sb.append(token);
            emailSender.poruka(sb.toString(),request.getMail());
            PorukaDTO poruka=new PorukaDTO("Zahtev za promenu sifre poslat na mail!");
            return new ResponseEntity<>(poruka, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(new ErrorDTO("Neuspesan pokusaj!"), HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping("resetLozinke")
    public ResponseEntity<?> resetLozinke(@RequestParam("token") String token, @RequestBody ResetPasswordDto request){
        try {
         Boolean promenjeno= userService.resetovanjeLozinke(token,request.getPassword());
         System.out.println("Controller za reset lozinke ==> "+token+" token + lozinka ==> "+request.getPassword());
         PorukaDTO poruka=new PorukaDTO();
            if(promenjeno){
                System.out.println("~~~~~LOZINKA PROMENJENA~~~~~");
                poruka=new PorukaDTO("Vasa lozinka je uspesno zamenjena novom!");
            }
            return new ResponseEntity<>(poruka, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(new ErrorDTO("Neuspesan pokusaj!"), HttpStatus.BAD_REQUEST);
        }

    }
    @GetMapping("podaciZaIzmenu")
    public ResponseEntity<KorisnikIzmenaDto> podaciZaIzmenu(@RequestHeader("Authorization") String token){
        System.out.println("TOKEN ####### TOKEN"+token.toString());
        KorisnikIzmenaDto korisnikIzmenaDto=userService.podaciZaIzmenu(token);
        return ResponseEntity.ok().body(
                korisnikIzmenaDto);
    }
    @PostMapping("izmenaPodataka")
    public ResponseEntity<?> izmenaArtikla(@RequestBody KorisnikIzmenaDto request ) {

        try {
            System.out.println("PROMENJENI PODACI KORISNIKA "+request.toString());
            userService.promenaKorisnika(request);
            PorukaDTO poruka=new PorukaDTO("Uspesno izmena!");
            return new ResponseEntity<>(poruka, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(new ErrorDTO("Neuspesna izmena!"), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/telegramPoruka")
    public  ResponseEntity<?> telegramPoruka(@RequestBody TelegramPorukaDto request) {
        try {
            System.out.println("Upao u slanje telegram poruke!");
            String telegramPoruka = request.getTelegramPoruka();
            telegramMessagesSender.sendToTelegram(telegramPoruka);
            PorukaDTO poruka=new PorukaDTO("PorukaPoslata!");
            return new ResponseEntity<>(poruka, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(new ErrorDTO("Neuspesna izmena!"), HttpStatus.BAD_REQUEST);

        }
    }
}


