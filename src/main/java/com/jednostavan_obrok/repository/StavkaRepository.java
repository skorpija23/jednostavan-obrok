package com.jednostavan_obrok.repository;

import com.jednostavan_obrok.model.Stavka;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StavkaRepository extends JpaRepository<Stavka,Integer> {
}
