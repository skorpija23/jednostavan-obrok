package com.jednostavan_obrok.service.impl;


import com.jednostavan_obrok.dto.KorisnikDTO;

import com.jednostavan_obrok.dto.KorisnikIzmenaDto;
import com.jednostavan_obrok.dto.LogovanjeKorisnikaDTO;
import com.jednostavan_obrok.dto.RegistracijaKorisnikaDTO;
import com.jednostavan_obrok.model.Korisnik;
import com.jednostavan_obrok.model.Passwordresettoken;
import com.jednostavan_obrok.model.Role;
import com.jednostavan_obrok.repository.KorisnikRepository;
import com.jednostavan_obrok.repository.ObrokRepository;
import com.jednostavan_obrok.repository.PasswordresettokenRepository;
import com.jednostavan_obrok.repository.RoleRepository;
import com.jednostavan_obrok.security.SecurityConfiguration;
import com.jednostavan_obrok.security.TokenUtils;
import com.jednostavan_obrok.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private SecurityConfiguration configuration;
    @Autowired
    private TokenUtils tokenUtils;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private KorisnikRepository korisnikRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private ObrokRepository obrokRepository;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Qualifier("userServiceImpl")
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private PasswordresettokenRepository passwordresettokenRepository;
    @Override
    @Transactional
    public Korisnik registracijaKupca(RegistracijaKorisnikaDTO request) throws Exception {
        Korisnik korisnik = new Korisnik();
        Role rola=roleRepository.findById(1).get();
        korisnik.setRole(rola);
        korisnik.setRole(roleRepository.findById(1).get());
        byte deaktiviran = 0;
        korisnik.setDeaktiviranje(deaktiviran);
        String ime = request.getIme();
        String prezime = request.getPrezime();
        String username = request.getUsername();
        String pass = configuration.passwordEncoder().encode(request.getPassword());
        int mobilni = Integer.parseInt(request.getMobilni());
        String mail = request.getEmail();
        String mesto = request.getMesto();
        String adresa = request.getAdresa();
        System.out.println(mesto + "Mesto " + "Adresa " + adresa);
        boolean proveraPodataka = true;
        if (ime != null && !ime.isEmpty()) {
            korisnik.setIme(ime);
        } else {
            proveraPodataka = false;
        }
        System.out.println("Boolean: " + proveraPodataka+" ime korisnika : "+ime);
        if (prezime != null && !prezime.isEmpty()) {
            korisnik.setPrezime(prezime);
        } else {
            proveraPodataka = false;
        }
        System.out.println("Boolean: " + proveraPodataka+"prezime korisnika : "+prezime);
        if (username != null && !username.isEmpty()) {
            korisnik.setUsername(username);
        } else {
            proveraPodataka = false;
        }
        System.out.println("Boolean: " + proveraPodataka +"username "+username);
        if (pass != null && !pass.isEmpty()) {
            korisnik.setPass(pass);
        } else {
            proveraPodataka = false;
        }
        System.out.println("Boolean: " + proveraPodataka+" lozinka "+pass);
        if (mobilni != 0) {
            korisnik.setMobilni(mobilni);
            System.out.println("moblni " + mobilni);
        } else {
            proveraPodataka = false;
        }
        System.out.println("Boolean: " + proveraPodataka+" mobilni "+mobilni);
        if (mail != null && !mail.isEmpty()) {
            korisnik.setMail(mail);
        } else {
            proveraPodataka = true;
        }
        if (mesto != null && !mesto.isEmpty()) {
            korisnik.setMesto(mesto);
            System.out.println("Mesto " + mesto);
        } else {
            proveraPodataka = true;
        }
        System.out.println("Boolean: " + proveraPodataka+" mesto "+mesto);
        if (adresa != null && !adresa.isEmpty()) {
            korisnik.setAdresa(adresa);
            System.out.println("Adresa " + adresa);
        } else {
            proveraPodataka = true;
        }
        System.out.println("Boolean: " + proveraPodataka+"adresa "+adresa);
        System.out.println("KOrisnik IZ servera sa istinom " + korisnik.toString() + proveraPodataka);
        if (proveraPodataka && korisnikRepository.pronadjiKorisnikaPoImenuPrezimenuUsername(ime) == null) {
            korisnikRepository.saveAndFlush(korisnik);
            System.out.println("Korisnik je uspesno unet u sistem ====>" + korisnik.toString());
        } else {
            throw new Exception("Doslo je do pogresnog unosa ili je preskoceno neko polje!");
        }

        return korisnik;
    }

    @Override
    public List<KorisnikDTO> korisnici() {
        List<Korisnik> listaKorisnika = korisnikRepository.findAll();
        List<KorisnikDTO> listaKorisnikaDto = new ArrayList<KorisnikDTO>();
        for (Korisnik korisnik : listaKorisnika) {
            KorisnikDTO korisnikDto = new KorisnikDTO();
            korisnikDto.setIdKorisnik(korisnik.getIdKorisnik());
            korisnikDto.setIme(korisnik.getIme());
            korisnikDto.setPrezime(korisnik.getPrezime());
            korisnikDto.setAdresa(korisnik.getAdresa());
            korisnikDto.setMesto(korisnik.getMesto());
            korisnikDto.setMobilni(String.valueOf(korisnik.getMobilni()));
            korisnikDto.setEmail(korisnik.getMail());
            korisnikDto.setDeaktiviranje(korisnik.getDeaktiviranje());
            listaKorisnikaDto.add(korisnikDto);
        }

        return listaKorisnikaDto;
    }

    @Override
    @Transactional
    public Boolean brisanjeKorisnika(int idKorisnik) {
        Korisnik korisnik = korisnikRepository.findById(idKorisnik).get();

        korisnikRepository.delete(korisnik);

        return true;

    }

    @Override
    public void deaktiviranjeKorisnika(int idKorisnik) {
        Korisnik korisnik = korisnikRepository.findById(idKorisnik).get();
        byte status = korisnik.getDeaktiviranje();
        if (status == 0) {
            byte promenjeniStatus = 1;
            korisnik.setDeaktiviranje(promenjeniStatus);
        } else {
            byte promenjeniStatus = 0;
            korisnik.setDeaktiviranje(promenjeniStatus);
        }
        korisnikRepository.save(korisnik);
    }


    @Override
    public Object logovanjeKorisnika(LogovanjeKorisnikaDTO request) throws Exception {
        String username = request.getUsername();
        String pass = request.getPassword();
        Object objekat = korisnikRepository.pronadjiUlogovanogKorisnika(username, pass);

        return objekat;
    }

    @Override
    public Korisnik findByUsername(String username) {
        Korisnik korisnik = korisnikRepository.findByUsername(username);
        return korisnik;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Korisnik korisnik = korisnikRepository.findByUsername(username);
        System.out.println("******* KORISNICKI SERVISE *******");
        System.out.println("&&&&&& KORISNIK IZ SERVISA " + korisnik.toString());
        System.out.println("&&&&&& KORISNIK IZ SERVISA " + korisnik.getRole().getRole());
        if (korisnik == null) {
            throw new UsernameNotFoundException("Korisnik nije pronadjen");
        }
        String role = "";
        if (korisnik.getRole().getRole().equals("korisnik")) {
            role = "KORISNIK";

        } else if (korisnik.getRole().getRole().equals("administrator")) {
            role = "ADMINISTRATOR";
        } else {
            throw new IllegalArgumentException("Korisnik nije validan");
        }

        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        grantedAuthorities.add(new SimpleGrantedAuthority(role));
        return new org.springframework.security.core.userdetails.User(korisnik.getUsername(), korisnik.getPass(), grantedAuthorities);
    }


    @Override
    public Korisnik findUserByEmail(String mail) {
       Korisnik korisnik=korisnikRepository.findByMail(mail);
        return korisnik;
    }

    @Override
    public void createPasswordResetTokenForUser(Korisnik korisnik, String token) {
        Passwordresettoken myToken = new Passwordresettoken();
        List<Passwordresettoken> listaTokena = passwordresettokenRepository.findAll();
        for (Passwordresettoken r : listaTokena) {
            if (r.getKorisnik().getIdKorisnik() == korisnik.getIdKorisnik()) {
                r.setToken(token);
                passwordresettokenRepository.save(r);
            }else{
                myToken.setToken(token);
                myToken.setKorisnik(korisnik);
                passwordresettokenRepository.save(myToken);
            }

        }
    }

    @Override
    public Boolean resetovanjeLozinke(String token, String password) {
        System.out.println("Upao u Servise RESET LOZINKE!!!");
        System.out.println("Token SERVISE "+token);
       Object obj=passwordresettokenRepository.findByToken(token);
        System.out.println("korisnik obj INTEGER === "+obj);
        if(obj!=null){
            int idKorisnika=Integer.parseInt(obj.toString());
            System.out.println("Id korisnika iz servisa je "+idKorisnika);
            Korisnik korisnik=korisnikRepository.findById(idKorisnika).get();
            System.out.println("Pronadjen korisnik u servisu je ==>"+korisnik);
            korisnik.setPass(passwordEncoder.encode(password));
            korisnikRepository.saveAndFlush(korisnik);

           return true;
        }else{
         return false;
        }

    }

    @Override
    public KorisnikIzmenaDto podaciZaIzmenu(String token) {
        String username = tokenUtils.getUsernameFromToken(token.split("\\s")[1]);
        Korisnik korisnik = korisnikRepository.findByUsername(username);
        KorisnikIzmenaDto korisnikIzmenaDto=new KorisnikIzmenaDto();
        korisnikIzmenaDto.setIme(korisnik.getIme());
        korisnikIzmenaDto.setPrezime(korisnik.getPrezime());
        korisnikIzmenaDto.setIdKorisnik(korisnik.getIdKorisnik());
        korisnikIzmenaDto.setAdresa(korisnik.getAdresa());
        korisnikIzmenaDto.setMesto(korisnik.getMesto());
        korisnikIzmenaDto.setMobilni(korisnik.getMobilni());
        korisnikIzmenaDto.setEmail(korisnik.getMail());
        korisnikIzmenaDto.setUsername(korisnik.getUsername());
        return korisnikIzmenaDto;
    }

    @Override
    public void promenaKorisnika(KorisnikIzmenaDto request) {
        System.out.println("service request "+request);
        Korisnik korisnik=korisnikRepository.findById(request.getIdKorisnik()).get();
        System.out.println("Pronadjen korisnik u servisu ====> "+korisnik);
        String staraLozinka=request.getStaraLozinka();
        String novaLozinka=request.getNovaLozinka();
        System.out.println("nova lozinka " +novaLozinka);
        boolean tacnaLozinka=true;

        boolean passwordMatch = passwordEncoder.matches(staraLozinka, korisnik.getPass());

        if(passwordMatch)
        {
            korisnik.setIme(request.getIme());
            korisnik.setPrezime(request.getPrezime());
            korisnik.setUsername(request.getUsername());
            korisnik.setPass(passwordEncoder.encode(novaLozinka));
            korisnik.setAdresa(request.getAdresa());
            korisnik.setMesto(request.getMesto());
            korisnik.setMobilni(request.getMobilni());
            korisnik.setMail(request.getEmail());
            System.out.println("Ukucana lozinka je ispravno ukucana i promenjena novom ==> "+novaLozinka.toString());

        } else {
            System.out.println("Ukucana lozinka nije ispravna!");
        }
        korisnikRepository.save(korisnik);
    }
}
