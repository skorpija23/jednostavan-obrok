package com.jednostavan_obrok.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the korisnik database table.
 * 
 */
@Entity
@NamedQuery(name="Korisnik.findAll", query="SELECT k FROM Korisnik k")
public class Korisnik implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)

	@Column(name="id_korisnik")
	private int idKorisnik;

	private String ime;
	private String prezime;
	private String adresa;
	private String mesto;
	private int mobilni;
	private String mail;
	private byte deaktiviranje;
	private String username;
	private String pass;



	//bi-directional many-to-one association to Role
	@ManyToOne
	private Role role;

	//bi-directional many-to-one association to Porudzbina
	@OneToMany(mappedBy="korisnik" , cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Porudzbina> porudzbinas;

	public Korisnik() {
	}
	public String getAdresa() {
		return this.adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}


	public byte getDeaktiviranje() {
		return this.deaktiviranje;
	}

	public void setDeaktiviranje(byte deaktiviranje) {
		this.deaktiviranje = deaktiviranje;
	}

	public int getIdKorisnik() {
		return this.idKorisnik;
	}

	public void setIdKorisnik(int idKorisnik) {
		this.idKorisnik = idKorisnik;
	}

	public String getIme() {
		return this.ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getMesto() {
		return this.mesto;
	}

	public void setMesto(String mesto) {
		this.mesto = mesto;
	}

	public int getMobilni() {
		return this.mobilni;
	}

	public void setMobilni(int mobilni) {
		this.mobilni = mobilni;
	}

	public String getPass() {
		return this.pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getPrezime() {
		return this.prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public List<Porudzbina> getPorudzbinas() {
		return this.porudzbinas;
	}

	public void setPorudzbinas(List<Porudzbina> porudzbinas) {
		this.porudzbinas = porudzbinas;
	}

	public Porudzbina addPorudzbina(Porudzbina porudzbina) {
		getPorudzbinas().add(porudzbina);
		porudzbina.setKorisnik(this);

		return porudzbina;
	}


	public Korisnik(int idKorisnik, String ime, String prezime, String adresa, String mesto, int mobilni, String mail, byte deaktiviranje, String username, String pass, Role role, List<Porudzbina> porudzbinas) {
		this.idKorisnik = idKorisnik;
		this.ime = ime;
		this.prezime = prezime;
		this.adresa = adresa;
		this.mesto = mesto;
		this.mobilni = mobilni;
		this.mail = mail;
		this.deaktiviranje = deaktiviranje;
		this.username = username;
		this.pass = pass;
		this.role = role;
		this.porudzbinas = porudzbinas;
	}

	public Porudzbina removePorudzbina(Porudzbina porudzbina) {
		getPorudzbinas().remove(porudzbina);
		porudzbina.setKorisnik(null);

		return porudzbina;
	}


}