package com.jednostavan_obrok.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the role database table.
 * 
 */
@Entity
@NamedQuery(name="Role.findAll", query="SELECT r FROM Role r")
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_role")
	private int idRole;

	private String role;


//	//bi-directional many-to-one association to Korisnik
	@OneToMany(mappedBy="role" , cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Korisnik> korisniks;

	public Role() {
	}

	public int getIdRole() {
		return this.idRole;
	}

	public void setIdRole(int idRole) {
		this.idRole = idRole;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}





	public Role(int idRole, String role) {
		this.idRole = idRole;
		this.role = role;

	}
}