package com.jednostavan_obrok.dto;

public class LogovanjeKorisnikaDTO {
    private String password;
    private String username;

    public LogovanjeKorisnikaDTO(String password, String username) {
        this.password = password;
        this.username = username;
    }

    public LogovanjeKorisnikaDTO() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "LogovanjeKorisnikaDTO{" +
                "password='" + password + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
