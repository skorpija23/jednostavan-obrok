package com.jednostavan_obrok;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
public class JednostavanObrokApplication {

	public static void main(String[] args) {
		SpringApplication.run(JednostavanObrokApplication.class, args);
	}
	public void run(String... args) throws Exception {
	}
}
