package com.jednostavan_obrok.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ForgotPassDto {

    private String mail;

}
