package com.jednostavan_obrok.repository;

import com.jednostavan_obrok.model.Korisnik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface KorisnikRepository extends JpaRepository<Korisnik,Integer> {


    @Query(value = " Select k.id_korisnik from korisnik as k  where k.username like  (?1) order by k.id_korisnik desc limit 1" ,
            nativeQuery = true)
    Object pronadjiIdKorisnikaPoUsernamu(String username);
    @Query(value = " Select * from korisnik as k where k.username like (?1)  "
            , nativeQuery = true)
    Object pronadjiKorisnikaPoImenuPrezimenuUsername(  String username);
    @Query(value = " Select * from korisnik as k where k.username like (?1) AND k.password like (?2)  "
            , nativeQuery = true)
    Object pronadjiUlogovanogKorisnika(String username, String pass);

    Korisnik findByUsername(String username);
    @Query(value = "Select ko.id_korisnik ,ko.kupac_id_kupac,ko.ime, ko.prezime, ko.blokiran,ku.adresa from korisnik as ko inner join\n" +
            "kupac as ku on ko.kupac_id_kupac = ku.id_kupac group by ko.id_korisnik ", nativeQuery = true)
    List<Object[]> pronadjiSveKupce();

    @Query(value = "Select ko.id_korisnik ,ko.prodavac_id_prodavac,ko.ime, ko.prezime, ko.blokiran,pr.adresa ,pr.naziv, pr.email, pr.poslujeOd from korisnik as ko inner join\n" +
            "prodavac as pr on ko.prodavac_id_prodavac = pr.id_prodavac group by ko.id_korisnik ", nativeQuery = true)
    List<Object[]> pronadjiSveProdavce();
    @Query(value = " Select * from korisnik as k where k.mail like (?1)  "
            , nativeQuery = true)

    Korisnik findByMail(String mail);


}