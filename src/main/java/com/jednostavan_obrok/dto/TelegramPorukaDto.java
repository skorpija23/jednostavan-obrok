package com.jednostavan_obrok.dto;

public class TelegramPorukaDto {
    private String telegramPoruka;

    public TelegramPorukaDto(String telegramPoruka) {
        this.telegramPoruka = telegramPoruka;
    }

    public TelegramPorukaDto() {
    }

    public String getTelegramPoruka() {
        return telegramPoruka;
    }

    public void setTelegramPoruka(String telegramPoruka) {
        this.telegramPoruka = telegramPoruka;
    }
}
