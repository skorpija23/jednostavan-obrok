package com.jednostavan_obrok.dto;

public class KompletnaPorudzbinaDto {

    private int idObroka;
    private String naziv;
    private double cena;
    private String velicina;
    private double cenaPutaKolicina;
    private int kolicina;

    public KompletnaPorudzbinaDto(int idObroka, String naziv, double cena, String velicina, double cenaPutaKolicina, int kolicina) {
        this.idObroka = idObroka;
        this.naziv = naziv;
        this.cena = cena;
        this.velicina = velicina;
        this.cenaPutaKolicina = cenaPutaKolicina;
        this.kolicina = kolicina;
    }

    public KompletnaPorudzbinaDto() {
    }

    public int getIdObroka() {
        return idObroka;
    }

    public void setIdObroka(int idObroka) {
        this.idObroka = idObroka;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    public String getVelicina() {
        return velicina;
    }

    public void setVelicina(String velicina) {
        this.velicina = velicina;
    }

    public double getCenaPutaKolicina() {
        return cenaPutaKolicina;
    }

    public void setCenaPutaKolicina(double cenaPutaKolicina) {
        this.cenaPutaKolicina = cenaPutaKolicina;
    }

    public int getKolicina() {
        return kolicina;
    }

    public void setKolicina(int kolicina) {
        this.kolicina = kolicina;
    }
}
